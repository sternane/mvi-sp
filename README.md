# Program na obarvování černobílých filmů pomocí konvolučních neuronových sítí 

Program dostane na vstupu černobílé video, video rozdělí na jednotlivé framy, zmenší je, obarví a opět z nich poskládá video. Z původního videa extrahuje i zvukovou stopu, kterou spojí s obarveným videem. Pro barvení používá neuronovou síť natrénovanou na dobarvování černobílých obrázků (http://richzhang.github.io/colorization/). Je možné volit mezi třemi natrénovanými modely. Model lze stáhnout v rámci jupyter notebooku.

Použití:
  * pro zkoušku stačí spustit všechny buňky notebooku
  * v části "colorization" lze změnit natrénovaný model
  * pro obarvení vlastního videa stačí zavolat funkci "colorize_video", které předáme parametrem relativní cestu k videu

Závislosti pro spuštění jupyter notebooku:
  * python 3.6
  * caffe (http://caffe.berkeleyvision.org/)
  * ffmpeg (apt install ffmpeg)
 
Použité knihovny pythonu (pip install <package_name>):
  * numpy
  * opencv (pip install opencv-python)
  * moviepy
  * numpy
  * pyplot
  * skimage
  * scipy
  * matplotlib


	




	


